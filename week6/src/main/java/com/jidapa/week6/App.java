package com.jidapa.week6;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        BookBank jidapa = new BookBank("Jidapa",50.0);
        
        jidapa.print();

        BookBank prayud = new BookBank("PraYuddd",100000.0);
        prayud.print();
        prayud.withdraw(40000.0);
        prayud.print();

        jidapa.deposit(40000.0);
        jidapa.print();

        BookBank prawit = new BookBank("Prawit",1000000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();



    }
}
