package com.jidapa.week6;

public class BookBank {
    private String name;
    private double balance;

    public BookBank(String name, double blance) {
        this.name = name;
        this.balance = blance;
    }

    public boolean deposit(double money) {
        this.balance = balance + money;
        return true;
    }

    public boolean withdraw(double money) {
        if (money < 1)
            return false;
        if (money > balance)
            return false;
        this.balance = balance - money;
        return true;
    }

    public void print() {
        System.out.println(name + " " + balance);
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

}
