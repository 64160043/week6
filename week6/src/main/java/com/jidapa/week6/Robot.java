package com.jidapa.week6;

public class Robot {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public static final int MIN_X = 0;
    public static final int MIN_y = 0;
    public static final int MAX_X = 19;
    public static final int MAX_Y = 19;

    public Robot(String name, char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }

    public Robot(String name, char symbol) {
        this(name,symbol,0,0);
    }

    public boolean up() {
        if (y == MIN_y)
            return false;
        this.y = this.y - 1;
        return true;
    }

    public boolean down() {
        if (y == MAX_Y)
            return false;
        y = y + 1;
        return true;
    }

    public boolean left() {
        x = x - 1;
        return true;
    }

    public boolean right() {
        x = x + 1;
        return true;
    }

    public void print() {
        System.out.println("Robot : " + name + "X:" + x + "Y:" + y);
    }

    public void setName(String name) {
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
