package com.jidapa.week6;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldDownOver(){
        Robot robot = new Robot("Robot", 'R',0,Robot.MAX_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());
    }
    @Test
    public void createRobotSucess1(){
        Robot robot = new Robot("Robot", 'R',10,11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }
    @Test
    public void createRobotSucess2(){
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }


    @Test
    public void shouldUpNegative(){
        Robot robot = new Robot("Robot", 'R',0,Robot.MIN_y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_y, robot.getY());
    }

    @Test
    public void shouldDownSuccess(){
        Robot robot = new Robot("Robot", 'R',0,0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }

    
    @Test
    public void shouldUpSuccess(){
        Robot robot = new Robot("Robot", 'R',0,1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }

}
